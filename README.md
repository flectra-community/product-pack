# Flectra Community / product-pack

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[sale_product_pack](sale_product_pack/) | 2.0.1.1.0| This module allows you to sell product packs
[stock_product_pack](stock_product_pack/) | 2.0.1.0.1| This module allows you to get the right available quantities of the packs
[product_pack](product_pack/) | 2.0.1.1.0| This module allows you to set a product as a Pack


